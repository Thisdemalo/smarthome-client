import {Lamp} from '../model/Lamp';
import { Observable } from 'rxjs';
import {DataService} from '../services/data.service';
import {Data} from '@angular/router';
import {RestService} from '../rest.service';
import {SocketService} from '../services/socket.service';
import {forEach} from '@angular/router/src/utils/collection';

/**
 * This class will contorl the exchanges between front and back ends
 */
export class LampControler {

  private lampes: Lamp[];
  private socketService: SocketService;


  constructor(private rest: RestService, private dataService: DataService, socketService: SocketService) {
    this.socketService = socketService;
  }


  /**
   * Notify BackEnd for any lamp changing
   */
  setLampById(lampe: Lamp): void {
    this.rest.setLight(lampe);
    this.socketService.sendMessage();
  }

  /**
   * Notify BackEnd for all lamps changing
   */
  setAllLamp(lampes: Lamp[]): void {
    lampes.forEach(lampe => {
      this.setLampById(lampe);
    });
    /*
    this.rest.setAllLamp(lampes);
    this.socketService.sendMessage();
    */
  }


  /**
   * Ask to back to send all lamps
   */

  getAllLamp(): Lamp[] {

    let res: any = [];

    /**** TEST ****/
    /*
    const light1 = new Lamp({id: 12, name: 'Test 1', piece: 'Cuisine', color: '#000000'});
    const light2 = new Lamp({id: 13, name: 'Test 2', piece: 'Salon', intensity: 50, color: '#125698'});
    const light3 = new Lamp({id: 14, name: 'Test 3', piece: 'Cuisine', intensity: 10, color: '#649731'});
    res.push(light1, light2, light3);
    */
    /**** END TEST ****/

    this.dataService.getLights();
    return this.dataService.AllLampes;

  }

}
