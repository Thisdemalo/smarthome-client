import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import {RequestOptions, ResponseContentType} from '@angular/http';
import { Lamp } from './model/lamp';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  private endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = 'http://' + location.hostname + ':8080/';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  /**
   * Get the authentication autorisation for a user
   */
  getAuthentication(userId: String, psw: String): Observable<any> {
    let res: any;

    const endpoint = this.endpoint + 'checkAuthentification';
    console.log(endpoint);

    
    res = this.http.post(endpoint, {login: userId, password: psw});

    return res;
  }



  /** LIGHT COMPONENT */
  getLights(): Observable<any> {

    let res: any;

    res = this.http.get(this.endpoint + 'getAllLamps').pipe(
      map(this.extractData));

    console.log(res);
    return res;
  }

  /**
   * Modify a single lamp
   * @param lamp The lamp to modify
   */
  setLight(lamp: Lamp) {
    console.log('Entry RestService : setLight')
    this.http
      .post(this.endpoint + 'setLampByID', lamp)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }


  setAllLamp(lampes: Lamp[]) {
    console.log(lampes);
    this.http
      .post(this.endpoint + 'setAllLamps', lampes)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

}
