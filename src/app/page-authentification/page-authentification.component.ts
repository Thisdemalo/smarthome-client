import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import * as sha256 from 'sha256';


@Component({
  selector: 'app-page-authentification',
  templateUrl: './page-authentification.component.html',
  styleUrls: ['./page-authentification.component.scss']
})
export class PageAuthentificationComponent implements OnInit {

  authStatus: boolean;
  displayErrorTxt = false;
  txtErrorFromBack = 'Une erreur est survenue pendant la connexion. Veuillez réessayer !';
  txtBadConnection = 'Vos identifiants de connexion sont erronés !';

  username = '';
  password = '';
  showSpinner = false;


  constructor(private authService: AuthService, private router: Router, public snackBar: MatSnackBar) { }



  /**
   * Call the backend to loggin
   */
  login() {

    this.showSpinner = true;

    this.authService.signIn(this.username, sha256(this.password)).subscribe((data: any) => {
      // TRAITEMENT
      this.showSpinner = false;
      console.log(data);
      this.authService.isAuth = data;

      if (this.authService.isAuth) {
        this.router.navigate(['accueil']);
        console.log('Authentification réussie !');
      } else {
        // On affiche un message d'erreur et on reste sur la page (vider les champs)
        console.log('Authentification echouée !');
        this.openSnackBar(this.txtBadConnection, null);
      }

    }, (error: any) => {
      console.log('ERROR WHILE CONNECTION WITH BACKEND.');
      this.showSpinner = false;

      this.openSnackBar(this.txtErrorFromBack, null);

    });

  }

  /**
   * Display an alert box
   * @param message
   * @param action
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  /*login() {
    this.showSpinner = true;
    if (this.username === 'admin' && this.password === 'admin') {
      this.authService.signIn().then(
        () => {
          console.log('connexion réussie');
          this.authStatus = this.authService.isAuth;
          this.router.navigate(['accueil']);
        }
      );
    } else {
      this.showSpinner = false;
    }
  }*/

  ngOnInit() {
    this.authStatus = this.authService.isAuth;
  }

}
