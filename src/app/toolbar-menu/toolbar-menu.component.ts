import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-toolbar-menu',
  templateUrl: './toolbar-menu.component.html',
  styleUrls: ['./toolbar-menu.component.scss']
})
export class ToolbarMenuComponent implements OnInit {
  typeConnexion = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.checkTypeConnexion();
  }

  connexionOrDeconnxion() {
    if (!this.authService.isAuth) {
      this.router.navigate(['auth']);
    } else {
      this.authService.signOut();
      if (!this.authService.isAuth) {
        this.router.navigate(['accueil']);
        this.checkTypeConnexion();
      }
    }
  }

  private checkTypeConnexion() {
    if (this.authService.isAuth) {
      this.typeConnexion = 'Déconnexion';
    } else {
      this.typeConnexion = 'Connexion';
    }
  }
}
