import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {DataService} from '../services/data.service';
import { SocketService } from '../services/socket.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-page-accueil',
  templateUrl: './page-accueil.component.html',
  styleUrls: ['./page-accueil.component.scss']
})
export class PageAccueilComponent implements OnInit {

  constructor(public snackBar: MatSnackBar, private authService: AuthService, private dataService: DataService, private socketService: SocketService) { }

  ngOnInit() {
    if (this.authService.isAuth) {
      console.log('connecté');
    } else {
      console.log('pas connecté');
    }

    this.socketService.actualScreen = this;

  }

  /**
   * Display an alert box
   * @param message The message to show
   * @param action Nothing
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
