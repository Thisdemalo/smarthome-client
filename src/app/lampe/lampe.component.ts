import {Component, OnInit, NgModule} from '@angular/core';
import {MatSliderChange, MatSnackBar} from '@angular/material';
import { MaterialModule } from '../material';

import { Lamp } from '../model/lamp';
import {forEach} from '@angular/router/src/utils/collection';
import {LampControler} from '../controler/lamp.controler';
import * as $ from 'jquery';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {DataService} from '../services/data.service';
import {RestService} from '../rest.service';
import {SocketService} from '../services/socket.service';



@NgModule({
  imports: []
})


@Component({
  selector: 'app-lampe',
  templateUrl: './lampe.component.html',
  styleUrls: ['./lampe.component.scss']
})



export class LampeComponent implements OnInit {

  /**** FIELDS ****/
  checkedAllLampes = false;
  allLampes: Lamp = new Lamp({id: 0, name: 'Tous', intensity: 0, color: '#ffffff'});
  controler: LampControler;
  showSpinner = true;

  constructor(public dataService: DataService, private authService: AuthService, private router: Router, private rest: RestService, private socketService: SocketService, public snackBar: MatSnackBar) {
    this.socketService.actualScreen = this;
  }

  ngOnInit() {
    if (!this.authService.isAuth) {
      this.router.navigate(['auth']);
    } else {
      this.controler = new LampControler(this.rest, this.dataService, this.socketService);

      this.dataService.getLights();
      if (this.dataService.AllLampes != null) {
        this.dataService.checkCheckedLampAll();
      }
      this.showSpinner = false;
    }
  }

  /**** FUNCTIONAL METHODS ****/

  /**
   * Notify controler when changes are occured
   * e: the id of the lamp
   */
  notifyChange(e: any) {
    if (e === -1) {
      this.controler.setAllLamp(this.dataService.AllLampes);
    } else {
      this.controler.setLampById(this.dataService.AllLampes.find(function(lampe) {
        return lampe.id === e;
      }));
    }
  }



  /**** UI METHODS ****/


  /** Détection de changement sur les slides toggle pour toutes les lumieres
   * Répercussion sur tous les slides toggles et les slider
   */
  changeSlideToggleAll() {
    if (this.dataService.checkedAllLampes) {
        this.dataService.AllLampe.intensity = 100;
        for (const lampe of this.dataService.AllLampes) {
          this.dataService.checkedLamp[lampe.id.toString()] = true;
        }
      } else {
        this.dataService.AllLampe.intensity = 0;
      for (const lampe of this.dataService.AllLampes) {
        this.dataService.checkedLamp[lampe.id.toString()] = false;
      }
    }

    this.changeSliderAll();
  }


  changeSliderAll() {
    if (this.dataService.AllLampe.intensity === 0) {
      this.dataService.checkedAllLampes = false;
      for (const lampe of this.dataService.AllLampes) {
        this.dataService.checkedLamp[lampe.id.toString()] = false;
      }
    } else {
      this.dataService.checkedAllLampes = true;
      for (const lampe of this.dataService.AllLampes) {
        this.dataService.checkedLamp[lampe.id.toString()] = true;
      }
    }

    for (const lampe of this.dataService.AllLampes) {
      lampe.intensity = this.dataService.AllLampe.intensity;
    }

    this.notifyChange(-1);
  }

  changeColorAll() {
    for (const lampe of this.dataService.AllLampes) {
      lampe.color = this.dataService.AllLampe.color;
    }
    this.notifyChange(-1);
  }


  /**
   * Détection de changement sur les slides toggle
   * Adapter le slider en conséquence
   */

  changeSlideToggleLamp(lamp: Lamp) {
    if (this.dataService.checkedLamp[lamp.id.toString()]) {
      lamp.intensity = 100;
    } else {
      lamp.intensity = 0;
    }

    this.dataService.checkCheckedLampAll();
    this.notifyChange(lamp.id);
  }

  changeSlideLamp(lamp: Lamp) {
    if (lamp.intensity === 0) {
      this.dataService.checkedLamp[lamp.id.toString()] = false;
    } else {
      this.dataService.checkedLamp[lamp.id.toString()] = true;
    }
    this.dataService.checkCheckedLampAll();
    this.notifyChange(lamp.id);
  }


  /**
   * Display an alert box
   * @param message The message to show
   * @param action Nothing
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  onKeydown(event, id) {
    if (event.key === "Enter") {
      this.notifyChange(id);
    }
  }

}
