import { BrowserModule } from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule} from './material';
import { ToolbarMenuComponent } from './toolbar-menu/toolbar-menu.component';
import { HomeComponent } from './home/home.component';
import {RouterModule, Routes} from '@angular/router';
import { PageStatistiquesComponent } from './page-statistiques/page-statistiques.component';
import { PageAccueilComponent } from './page-accueil/page-accueil.component';
import {FormsModule} from '@angular/forms';
import { PageAuthentificationComponent } from './page-authentification/page-authentification.component';
import { LampeComponent } from './lampe/lampe.component';
import { HttpClientModule } from '@angular/common/http';
import { ColorPickerModule } from 'ngx-color-picker';
import {AuthService} from './services/auth.service';
import {FlexLayoutModule} from '@angular/flex-layout';
import { ObjectsConnectesComponent } from './objects-connectes/objects-connectes.component';
import { DataService } from './services/data.service';
import {SocketService} from './services/socket.service';




const appRoutes: Routes = [
  {
    path: 'statistiques',
    component: PageStatistiquesComponent,
    data: { title : 'Statistiques' }
  },
  {
    path: 'lampe',
    component: LampeComponent,
    data: { title : 'Lampes' }
  },
  {
    path: 'objectsConnectes',
    component: ObjectsConnectesComponent,
    data: { title : 'Objets connectés' }
  },
  {
    path: 'auth',
    component: PageAuthentificationComponent
  },
  {
    path: '',
    redirectTo: '/accueil',
    pathMatch: 'full'
  },
  {
    path: 'accueil',
    component: PageAccueilComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarMenuComponent,
    HomeComponent,
    PageStatistiquesComponent,
    PageAccueilComponent,
    PageAuthentificationComponent,
    LampeComponent,
    ObjectsConnectesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ColorPickerModule,
    FlexLayoutModule
  ],
  providers: [
    AuthService,
    DataService,
    SocketService
  ],
  bootstrap: [AppComponent]
})

@Injectable({
  providedIn: 'root'
})

export class AppModule {
  constructor(public socketService: SocketService) {

  }
}
