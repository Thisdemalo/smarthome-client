import { RestService } from "../rest.service";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuth = false;

  /**
   * 
   * @param rest The rest service
   */
  constructor(private rest: RestService) {

  }


  /**
   * 
   * @param user 
   * @param mdp 
   */
  signIn(user: String, mdp: String): Observable<any> {
    return this.rest.getAuthentication(user, mdp);
  }

  /*signIn() {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            this.isAuth = true;
            resolve(true);
          }, 200
        );
      }
    );
  }*/

  signOut() {
    this.isAuth = false;
  }
}
