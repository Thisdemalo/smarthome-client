import {Observable, of} from 'rxjs';
import {Lamp} from '../model/lamp';
import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {RestService} from '../rest.service';


// @ts-ignore
@Injectable({
  providedIn: 'root'
})

export class DataService implements OnInit {


  constructor(private rest: RestService) {
  }

  AllLampes: Lamp[] = null;
  AllLampe: Lamp = new Lamp({id: 0, name: 'Tous', intensity: 0, color: '#ffffff'});
  checkedAllLampes = false;
  nbLampes: number = 0;
  checkedLamp: { [id: string]: boolean} = {};

  ngOnInit(): void {
    this.getLights();
  }


  getLights() {
    this.rest.getLights().subscribe((data: [Lamp]) => {
      this.AllLampes = data;
      if (this.AllLampes != null)
      for (const lampe of this.AllLampes) {
        if (lampe.intensity > 0) {
          this.checkedLamp[lampe.id.toString()] = true;
        } else {
          this.checkedLamp[lampe.id.toString()] = false;
        }
      }
      if (this.AllLampes != null) {
        this.nbLampes = this.AllLampes.length;
        this.checkCheckedLampAll();
      }

    });
  }

  checkCheckedLampAll() {
    if (this.AllLampes != null) {
      let isOn = true;
      let maxIntensity: Number = 0;
      for (let lampe of this.AllLampes) {
        if (lampe.intensity == 0) {
          isOn = false;
        }
        if (lampe.intensity > maxIntensity) {
          maxIntensity = lampe.intensity;
        }
      }

      this.checkedAllLampes = isOn;
      this.AllLampe.intensity = maxIntensity;
    }

  }
}
