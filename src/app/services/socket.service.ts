import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {DataService} from './data.service';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private endpoint: string;
  private stompClient: any;
  public actualScreen: any = null;


  constructor(private http: HttpClient, public dataService: DataService) {

    console.log('SocketService: Entering constructor');

    this.endpoint = 'http://' + location.hostname + ':8080/';

    this.initializeWebSocketConnection();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
  }

  /**
   * Initialize the Web Socket process
   */
  initializeWebSocketConnection(): void {
    const ws = new SockJS(this.endpoint + '/notify');
    this.stompClient = Stomp.over(ws);
    const that = this;
    this.stompClient.connect({}, function(frame) {
      //console.log('connected: ' + frame);
      that.stompClient.subscribe('/objects/update', 
        (message) => {
          if (message.body) {
            // We just call our own dataservice to update persistant data
            that.dataService.getLights();
            if (that.actualScreen != undefined && that.actualScreen != null) {
              that.actualScreen.openSnackBar('Rechargement des données', null);
            }
          }
      }, (error) => {
        if (that.actualScreen != undefined && that.actualScreen != null) {
          that.actualScreen.openSnackBar('Connexion avec le serveur perdue, tentative...', null);
        }
      });
    });
  }


  /**
   * Send to backend that there are some modifications
   * @param message 
   */
  sendMessage(): void {
    //console.log('Sending message : ' + message);
    this.stompClient.send('/app/update', {});
  }


}
