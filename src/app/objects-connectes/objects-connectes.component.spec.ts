import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectsConnectesComponent } from './objects-connectes.component';

describe('ObjectsConnectesComponent', () => {
  let component: ObjectsConnectesComponent;
  let fixture: ComponentFixture<ObjectsConnectesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectsConnectesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectsConnectesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
